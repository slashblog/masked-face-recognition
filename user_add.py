from embedding import Embedding, MaskedEmbedding
from face import FaceNotFoundException
from multi import MultiProcess, SingleProcess
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Normalizer
from train import SVCTrain
from tensorflow.keras.models import load_model

import argparse
import numpy
import os
import sys

class CreateUserEmbedding:
    def __init__(self, use_myriad, masked):
        self._EMBEDDINGS = 'embeddings'
        self._LABELS = 'labels'
        self.use_myriad = use_myriad
        self.masked = masked

    def _load_faces(self, directory, embedder):
        faces = list()
        for filename in os.listdir(directory):
            path = os.path.join(directory, filename)
            try:
                masked_faces = embedder.get(path)
                faces.extend(masked_faces)
            except FaceNotFoundException:
                print('No face found in ' + path)
        return faces

    # load a dataset that contains one subdir for each class that in turn contains images
    def _get_embeddings(self, directory):
        processor = SingleProcess() if self.use_myriad else MultiProcess() 
        X, y = list(), list()

        def _handle_embedding(embedding):
            X.extend(embedding[self._EMBEDDINGS])
            y.extend(embedding[self._LABELS])
            print('Embedding count: %d, label count: %d' % (len(X), len(y)))

        def _load_directories(subdirs):
            if self.masked:
                embedder = Embedding()
            else:
                embedder = MaskedEmbedding(self.use_myriad)

            for subdir in subdirs:
                # path
                path = os.path.join(directory, subdir)
                # skip any files that might be in the dir
                if not os.path.isdir(path):
                    continue
                # load all faces in the subdirectory
                embeddings = self._load_faces(path, embedder)

                images_count = len(os.listdir(path))
                print('For ' + subdir + ' generated ' + str(len(embeddings)) + ' masked faces out of ' + str(images_count) + ' input images')
                # create labels
                labels = [subdir for _ in range(len(embeddings))]

                processor.enqueue({self._EMBEDDINGS: embeddings, self._LABELS: labels})
                # summarize progress
                print('> [%d] loaded %d examples for class: %s' % (os.getpid(), len(embeddings), subdir))

            print('No more tasks for process [%d]. Exiting' % os.getpid())
            processor.finish()


        subdirs = os.listdir(directory)
        processor.run(_load_directories, _handle_embedding, subdirs)

        print('Total faces: %d, labels: %d' % (len(X), len(y)))
        return numpy.asarray(X), numpy.asarray(y)

    def process_and_save_embeddings(self, train_directory, test_directory, embedding_path):
        trainX, trainy = self._get_embeddings(train_directory)
        testX, testy = self._get_embeddings(test_directory)
        numpy.savez_compressed(embedding_path, trainX, trainy, testX, testy)

    def _process_images_and_get_embeddings(self, images, use_myriad):
        processor = SingleProcess() if use_myriad else MultiProcess()
        faces = list()

        def _handle_embedding(embedding):
            faces.extend(embedding)
            print('Embedding count: %d, label count: %d' % (len(X), len(y)))

        def _load_images(images_group):
            embedder = MaskedEmbedding(use_myriad)
            for image in images_group:
                masked_faces = embedder.get(image)
                processor.enqueue(masked_faces)

            print('No more tasks for process [%d]. Exiting' % os.getpid())
            processor.finish()

        processor.run(_load_images, _handle_embedding, images)

        print('Total faces: %d' % len(faces))
        return faces

    def process_images_and_save_user_embedding(self, user, images, embedding_path, use_myriad):
        faces = self._process_images_and_get_embeddings(images, use_myriad)

        if len(faces) == 0:
            print('No faces found in the images supplied. Skipping adding embedding')
            sys.exit(1)

        faces_train = faces
        faces_test = list()
        #if len(faces) <= 8:
        #    faces_train = faces
        #    faces_test = list()
        #else:
        #    temp = int(0.80 * (len(faces) / 4))
        #    faces_train = faces[0:temp]
        #    faces_test = faces[temp:]

        labels_train = [user for _ in range(len(faces_train))]
        labels_test = [user for _ in range(len(faces_test))]

        print('Faces for %s: train: %d, test: %d' % (user, len(faces_train), len(faces_test)))

        data = numpy.load(embedding_path)
        trainX, trainy, testX, testy = data['arr_0'], data['arr_1'], data['arr_2'], data['arr_3']

        # normalize input vectors
        in_encoder = Normalizer(norm='l2')
        trainX = in_encoder.transform(trainX)
        testX = in_encoder.transform(testX)

        trainX = trainX.tolist()
        trainy = trainy.tolist()
        testX = testX.tolist()
        testy = testy.tolist()

        trainX.extend(faces_train)
        trainy.extend(labels_train)
        testX.extend(faces_test)
        testy.extend(labels_test)

        trainX = numpy.asarray(trainX)
        trainy = numpy.asarray(trainy)
        testX = numpy.asarray(testX)
        testy = numpy.asarray(testy)

        numpy.savez_compressed(embedding_path, trainX, trainy, testX, testy)

def create_embeddings(directory, embedding_path, train, val, use_myriad, masked):
    train_directory = os.path.join(directory, train)
    test_directory = os.path.join(directory, val)
    CreateUserEmbedding(use_myriad, masked).process_and_save_embeddings(train_directory, test_directory, embedding_path)

def train_embeddings(embedding_path, model_path):
    SVCTrain().train(embedding_path, model_path)

def create_and_train_embeddings(directory, train, val, overwrite, use_myriad, masked):
    my_path = os.path.dirname(__file__)
    embedding_path = os.path.join(my_path, directory + '-embeddings.npz')
    model_path = os.path.join(my_path, directory + '-model')
    directory = os.path.join(my_path, directory)

    def check_file(file_path, file_type):
        if os.path.exists(file_path):
            if overwrite:
                print('Warn: ' + file_type + ' file: ' + file_path + ' already exists. It will get overwritten.')
            else:
                print('Error: ' + file_type + ' file: ' + file_path + ' already exists.')
                sys.exit(1)

    if not os.path.exists(directory) or not os.path.isdir(directory):
        print('Error: directory not found: ' + directory)
        sys.exit(1)

    check_file(embedding_path, 'Embedding')
    check_file(model_path, 'Model')

    create_embeddings(directory, embedding_path, train, val, use_myriad, masked)
    train_embeddings(embedding_path, model_path)

def create_and_train_user_embedding(embed, user, images, use_myriad, masked):
    my_path = os.path.dirname(__file__)
    embedding_path = os.path.join(my_path, embed + '-embeddings.npz')
    model_path = os.path.join(my_path, embed + '-model')

    if not os.path.exists(embedding_path):
        print('Error: Embedding file: ' + embedding_path + ' does not exist.')
        sys.exit(1)

    if not os.path.exists(model_path) or not os.path.exists(model_path + '-classes.npy'):
        print('Error: Model file ' + model_path + ' does not exist')
        sys.exit(1)

    CreateUserEmbedding(use_myriad, masked).process_images_and_save_user_embedding(user, images, embedding_path, use_myriad)
    train_embeddings(embedding_path, model_path)

parser = argparse.ArgumentParser(description='Add user embedding and train for user')
parser.add_argument('--myriad', required=False, default=False, action='store_true', help='Use Intel Movidius Neural Compute Stick')
parser.add_argument('--masked', required=False, default=False, action='store_true', help='Signifies that the input training images are masked')

subparsers = parser.add_subparsers(help='sub command help')

user_parser = subparsers.add_parser('user', help='user help')
user_parser.add_argument('user', type=str, help='User name')
user_parser.add_argument('embed', type=str, help='Model and Embedding identifier')
user_parser.add_argument('images', type=str, nargs='+', help='Image paths')

embed_parser = subparsers.add_parser('embed', help='embed help')
embed_parser.add_argument('embed', type=str, help='Folder containing images as per the hierarchy. Also used to generate embeddings and model file names.')
embed_parser.add_argument('--train-directory', type=str, required=False, default='train', help='Train image subdirectory')
embed_parser.add_argument('--val-directory', type=str, required=False, default='val', help='Validation image subdirectory')
embed_parser.add_argument('--force-overwrite', required=False, action='store_true', help='Force overwriting of embeddings and model files.')

args = parser.parse_args()

if not hasattr(args, 'embed') and not hasattr(args, 'user'):
    print('Error: either user or embed expected.')
    exit(1)

embed = args.embed

if hasattr(args, 'user'):
    create_and_train_user_embedding(embed, args.user, args.images, args.myriad, args.masked)
else:
    create_and_train_embeddings(embed, args.train_directory, args.val_directory, args.force_overwrite, args.myriad, args.masked)
