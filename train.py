from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Normalizer
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score
from embedding import Embedding, MaskedEmbedding

import joblib
import numpy
import os

class SVCTrain:
    def __init__(self):
        pass

    def train(self, emb_path, model_path):
        data = numpy.load(emb_path)
        trainX, trainy, testX, testy = data['arr_0'], data['arr_1'], data['arr_2'], data['arr_3']

        print('Training embedding count: %d, label count: %d' % (len(trainX), len(trainy)))
        print('Training labels: ' + str(numpy.unique(trainy)))
        print('Testing embedding count: %d, label count: %d' % (len(testX), len(testy)))
        print('Testing labels: ' + str(numpy.unique(testy)))

        should_test = False if len(testX) == 0 else True

        # normalize input vectors
        in_encoder = Normalizer(norm='l2')
        trainX = in_encoder.transform(trainX)

        if should_test:
            testX = in_encoder.transform(testX)
        
        # label encode targets
        out_encoder = LabelEncoder()
        out_encoder.fit(trainy)
        trainy = out_encoder.transform(trainy)

        if should_test:
            testy = out_encoder.transform(testy)

        #print(trainy)
        #print(testy)
        
        # fit model
        print('Training model starting')
        model = SVC(kernel='linear', probability=True)
        model.fit(trainX, trainy)
        print('Training model done')

        if not should_test:
            print('Skipping testing as no test data exists')
        elif len(numpy.unique(testy)) > 1:
            print('Testing data ...')
            scores = cross_val_score(model, testX, testy)
            print('Testing done: Accuracy: %0.2f (+/- %0.2f)' % (scores.mean(), scores.std() * 2))
        else:
            print('Skipping testing as there is only one test label')

        #for selection in range(1, len(testX)):
            ## test model on a random example from the test dataset
            ##selection = choice([i for i in range(testX.shape[0])])
            ##random_face_pixels = testX_faces[selection]
            #random_face_emb = testX[selection]
            #random_face_class = testy[selection]
            #random_face_name = out_encoder.inverse_transform([random_face_class])
            ## prediction for the face
            #samples = numpy.expand_dims(random_face_emb, axis=0)
            #yhat_class = model.predict(samples)
            #yhat_prob = model.predict_proba(samples)
            ## get name
            #class_index = yhat_class[0]
            #class_probability = yhat_prob[0,class_index] * 100
            #predict_names = out_encoder.inverse_transform(yhat_class)
            #print('Predicted: %s (%.3f)' % (predict_names[0], class_probability))
            #print('Expected: %s' % random_face_name[0])

    
        print('Saving model to: ' + model_path)
        joblib.dump(model, model_path)
        numpy.save(model_path + '-classes', out_encoder.classes_)
        print('Saving model done')


if __name__ == '__main__':
    my_path = os.path.dirname(__file__)
    directory = 'images'
    embedding_path = os.path.join(my_path, directory + '-embeddings.npz')
    model_path = os.path.join(my_path, directory + '-model')

    SVCTrain().train(embedding_path, model_path)