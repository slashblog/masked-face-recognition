import face_recognition
import matplotlib
import os

from matplotlib import pyplot

matplotlib.use('TkAgg')

image_paths = [
    'pete-sampras.jpg',
]

for image_path in image_paths:
    face_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), image_path)

    face_image_np = face_recognition.load_image_file(face_path)
    pyplot.imshow(face_image_np)
    pyplot.show()

    face_locations = face_recognition.face_locations(face_image_np, model='cnn')
    print('Face locations:')
    print(face_locations)

    for face_location in face_locations:
        top, right, bottom, left = face_location
        face_pixels = face_image_np[top:bottom, left:right]
        pyplot.imshow(face_pixels)
        pyplot.show()

    face_landmarks = face_recognition.face_landmarks(face_image_np, face_locations)
    print('Face landmarks:')
    print(face_landmarks)

    pyplot.imshow(face_image_np)
    for face_landmark in face_landmarks:
        for landmark in face_landmark:
            #print(landmark + ':')
            for y, x in face_landmark[landmark]:
                pyplot.scatter(y, x, color='r')
    pyplot.show()

