from multiprocessing import Process, Lock, Queue, Value
from multiprocessing import cpu_count

import os
import random
import signal
import sys
import time

class SingleProcess:
    def __init__(self):
        self.q = list()

    def enqueue(self, item):
        self.q.append(item)

    def finish(self):
        pass

    def run(self, target_function, handler_function, arguments):
        target_function(arguments)

        while len(self.q) > 0:
            handler_function(self.q.pop(0))

class MultiProcess(SingleProcess):
    def __init__(self):
        self.l = Lock()
        self.q = Queue()
        self.c = Value('I')
        self.c.value = 0

    def enqueue(self, item):
        self.q.put(item)
        #print('q item count: %d' % self.q.qsize())

    def finish(self):
        self.l.acquire()
        try:
            print('Finished process: ' + str(os.getpid()))
            self.c.value += 1
        finally:
            self.l.release()

    def _target_function(self, target_function, argument_group):
        print('Started process: ' + str(os.getpid()))
        def signal_handler(sig, frame):
            print('Exiting from child process on user request: ' + str(os.getpid()))
            sys.exit(0)

        signal.signal(signal.SIGINT, signal_handler)

        target_function(argument_group)

    def run(self, target_function, handler_function, arguments):
        processes = list()

        def signal_handler(sig, frame):
            print('You pressed Ctrl+C! Will exit and kill any child processes!')
            for process in processes:
                if process.pid is None:
                    os.kill(process.pid, signal.SIGTERM)
            sys.exit(0)

        count = cpu_count()
        print('CPU count: %d' % count)

        length = len(arguments)
        if length <= count:
            argument_groups = []
            for argument in arguments:
                argument_groups.append([argument])
            count = length
        else:
            argument_groups = [ arguments[i*length // count: (i+1)*length // count] for i in range(count) ]

        for argument_group in argument_groups:
            #print(argument_group)
            p = Process(target=self._target_function, args=[target_function, argument_group])
            processes.append(p)
            p.start()

        signal.signal(signal.SIGINT, signal_handler)

        processes_finished = False

        while not processes_finished:
            time.sleep(1)
            self.l.acquire()
            try:
                #print('process count value %d' % (self.c.value))
                if self.c.value == count:
                    processes_finished = True	
            finally:
                self.l.release()

            while self.q.empty() == False:
                try:
                    #print('Trying to pick item from queue')
                    queue_item = self.q.get(block=True, timeout=1)
                    #print('Picked item from queue')
                    handler_function(queue_item)
                except:
                    #print('Timeout occured waiting for queue item')
                    break

if __name__ == '__main__':
    mp = MultiProcess()

    items = list()
    def sleep_and_queue(argument_group):
        for argument in argument_group:
            time.sleep(random.randint(1,3))
            #print('Enqueue ' + argument)
            mp.enqueue(argument)
        mp.finish()
    
    def handler(item):
        items.append(item)

    randomlist = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    mp.run(sleep_and_queue, handler, randomlist)
    print(str(os.getpid()) + ':')
    print(items)
