from face import Face, MaskedFace, MyriadFace
from tensorflow.keras.models import load_model

import numpy

class Embedding:
    def __init__(self):
        self.face = self._get_face()
        self.model = load_model('models/keras-facenet/model/facenet_keras.h5')

    def _get_face(self):
        #print('Returning Face')
        return Face()

    def _get_embedding(self, face_pixels):
        # scale pixel values
        face_pixels = face_pixels.astype('float32')
        # standardize pixel values across channels (global)
        mean, std = face_pixels.mean(), face_pixels.std()
        face_pixels = (face_pixels - mean) / std
        # transform face into one sample
        samples = numpy.expand_dims(face_pixels, axis=0)
        # make prediction to get embedding
        yhat = self.model.predict(samples)
        return yhat[0]

    def get(self, image):
        embeddings = list()
        for face_pixels in self.face.get(image):
            embeddings.append(self._get_embedding(face_pixels))

        return embeddings

class MyriadEmbedding(Embedding):
    def __init__(self):
        super().__init__()

    def _get_face(self):
        return MyriadFace()

class MaskedEmbedding(Embedding):
    use_myriad = False

    def __init__(self, use_myriad):
        self.use_myriad = use_myriad
        super().__init__()

    def _get_face(self):
        #print('Returning MaskedFace')
        print('Masked face will be generated using myriad => ' + str(self.use_myriad))
        return MaskedFace(self.use_myriad)
