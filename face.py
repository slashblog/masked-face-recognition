from abc import ABC, abstractmethod
from PIL import Image, ImageOps

import cv2
import numpy
import os
import time

class FaceBase(ABC):
    REQUIRED_SIZE = (160, 160)

    @abstractmethod
    def get(self, image):
        pass

    @staticmethod
    def _get_image_pixels(filename):
        # load image from file
        image = Image.open(filename)
        image = ImageOps.exif_transpose(image)
        # convert to RGB, if needed
        image = image.convert('RGB')
        # convert to array
        return numpy.asarray(image)

class Face(FaceBase):
    def __init__(self):
        from mtcnn.mtcnn import MTCNN
        self._detector = MTCNN()

    def get_face(self, image):
        start = time.time()
        if type(image) == str:
            print('Processing ' + image)
            pixels = self._get_image_pixels(image)
        else:
            pixels = image
        end = time.time()
        print(f'Load took: {end - start}')
    
        start = time.time()
        results = self._detector.detect_faces(pixels)
        end = time.time()
        print(f'Face detection took: {end - start}')

        if len(results) == 0:
            print('No faces found')
            raise FaceNotFoundException

        start = time.time()
        # extract the bounding box from the first face
        x1, y1, width, height = results[0]['box']
        # bug fix
        x1, y1 = abs(x1), abs(y1)
        x2, y2 = x1 + width, y1 + height
        # extract the face
        face = pixels[y1:y2, x1:x2]
        # resize pixels to the model size
        image = Image.fromarray(face)
        image = image.resize(self.REQUIRED_SIZE)

        end = time.time()
        print(f'Face extraction from image took: {end - start}')

        return image

    def get(self, image):
        image = self.get_face(image)
        return [numpy.asarray(image)]


class MyriadFace(FaceBase):
    _myriad_base_path = 'models/myriad/face-detection-retail-0004'
    _myriad_model_xml_path = _myriad_base_path + '.xml'
    _myriad_model_bin_path = _myriad_base_path + '.bin'
    cur_request_id = 0
    max_cur_request_id = 10000
    detection_threshold = 0.4

    def __init__(self):
        try:
            from openvino.inference_engine import IENetwork, IECore
        except:
            print('Please make sure your OpenVINO environment variables are set by sourcing the setupvars.sh script found in <your OpenVINO install location>/bin/ folder.')
            exit(1)

        ie = IECore()
        face_net = IENetwork(model = self._myriad_model_xml_path, weights = self._myriad_model_bin_path)

        count = 0
        while count < 3:
            try:
                self.face_exec_net = ie.load_network(network = face_net, device_name = "MYRIAD")
                break
            except:
                count = count + 1
                if count < 3:
                    print('Failed to load network. Retrying')
                else:
                    print('Failed to load network. Retries exhausted. Exiting')
                    exit(1)

        # Get the input and output node names
        self.face_input_blob = next(iter(face_net.inputs))
        self.face_output_blob = next(iter(face_net.outputs))
        
        # Get the input and output shapes from the input/output nodes
        self.face_input_shape = face_net.inputs[self.face_input_blob].shape
        self.face_output_shape = face_net.outputs[self.face_output_blob].shape

    def get_face(self, image):
        start = time.time()

        faces_found = False
        self.cur_request_id = self.cur_request_id % self.max_cur_request_id

        face_n, face_c, face_h, face_w = self.face_input_shape
        #face_x, face_y, face_detections_count, face_detections_size = self.face_output_shape

        if type(image) == str:
            frame = cv2.imread(image)
        else:
            frame = image
        image_w = frame.shape[1]
        image_h = frame.shape[0]
    
        if frame is None:
            print('Unable to read the image file: %s' % image)
            quit()

        pixels = cv2.resize(frame, (face_w, face_h))
        pixels = numpy.transpose(pixels, (2, 0, 1))
        pixels = pixels.reshape((face_n, face_c, face_h, face_w))

        end = time.time()
        print(f'Load took: {end - start}')

        start = time.time()
        self.face_exec_net.start_async(request_id=self.cur_request_id, inputs={self.face_input_blob: pixels})
    
        if self.face_exec_net.requests[self.cur_request_id].wait(-1) == 0:
            # get the inference result
            inference_results = self.face_exec_net.requests[self.cur_request_id].outputs[self.face_output_blob]
            end = time.time()
            print(f'Face detection took: {end - start}')
            
            for face_num, detection_result in enumerate(inference_results[0][0]):
                # Draw only detection_resultects when probability more than specified threshold
                if detection_result[2] > self.detection_threshold:
                    print('Image detection result: %f out of %f' % (detection_result[2], self.detection_threshold))
                    start = time.time()

                    box_left = int(detection_result[3] * image_w)
                    box_top = int(detection_result[4] * image_h)
                    box_right = int(detection_result[5] * image_w)
                    box_bottom = int(detection_result[6] * image_h)
                    #class_id = int(detection_result[1])

                    print('box: %d:%d, %d:%d' % (box_top, box_bottom, box_left, box_right))
                    if box_top < 0 or box_bottom < 0 or box_left < 0 or box_right < 0:
                        continue
                    
                    faces_found = True
                    cropped_face = frame[box_top:box_bottom, box_left:box_right]
                    face_image_path = '/tmp/%d-%f.jpg' % (os.getpid(), time.time())
                    cv2.imwrite(face_image_path, cropped_face)

                    ret = self._get_image_pixels(face_image_path)
                    ret = Image.fromarray(ret)
                    os.remove(face_image_path)
                    end = time.time()
                    print(f'Face extraction from image took: {end - start}')
                    return ret.resize(self.REQUIRED_SIZE)
                else:
                    print('Face detection threshold not met: %f/%f' % (detection_result[2], self.detection_threshold))
                    break

        if not faces_found:
            raise FaceNotFoundException
    
    def get(self, image):
        return [numpy.asarray(self.get_face(image))]

class MaskedFace(FaceBase):
    def __init__(self, use_myriad):
        self.face = MyriadFace() if use_myriad else Face()
        
        IMAGE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'masks')
        DEFAULT_IMAGE_PATH = os.path.join(IMAGE_DIR, 'default-mask.png')
        BLACK_IMAGE_PATH = os.path.join(IMAGE_DIR, 'black-mask.png')
        BLUE_IMAGE_PATH = os.path.join(IMAGE_DIR, 'blue-mask.png')
        #RED_IMAGE_PATH = os.path.join(IMAGE_DIR, 'red-mask.png')
        #self.MASK_PATHS = [DEFAULT_IMAGE_PATH, BLACK_IMAGE_PATH, BLUE_IMAGE_PATH, RED_IMAGE_PATH]
        self.MASK_PATHS = [DEFAULT_IMAGE_PATH, BLACK_IMAGE_PATH, BLUE_IMAGE_PATH]

    def get(self, image):
        from mask import FaceMasker
        image = self.face.get_face(image)
        
        #temp_save_path = os.path.join('/tmp', 'temp-' + str(os.getpid()) + '.jpg')
        temp_save_path = os.path.join('/tmp', 'temp-' + str(os.getpid()) + '-' + str(time.time()) + '.jpg')
        image.save(temp_save_path)

        masked_faces = list()
        for mask_path in self.MASK_PATHS:
            save_path = os.path.splitext(temp_save_path)[0] + '-masked.jpg'
            FaceMasker(temp_save_path, mask_path, True, 'cnn', save_path).mask()
            
            if os.path.exists(save_path):
                masked_faces.append(numpy.asarray(self._get_image_pixels(save_path)))
                os.remove(save_path)
            else:
                break

        os.remove(temp_save_path)

        return masked_faces

class FaceNotFoundException(Exception):
    pass
