import argparse
import gpiozero
import os
import time

from recognize import SVCFaceRecognizerForCamera

parser = argparse.ArgumentParser(description='Face recognition based door opener')
parser.add_argument('--green-pin', '-g', type=int, help='Green pin number', required=True, choices=range(1, 41))
parser.add_argument('--red-pin', '-r', type=int, help='Red pin number', required=True, choices=range(1, 41))
parser.add_argument('--camera', '-c', type=int, help='Camera index', required=False, default=0, choices=range(0, 10))
parser.add_argument('--match-percentage', type=int, help='Percentage above which it is considered a match', required=False, default=50, choices=range(0,101))

args = parser.parse_args()

green_pin = args.green_pin
red_pin = args.red_pin
cam_index = args.camera

model_path = os.path.join(os.path.dirname(__file__), 'images-model')
recognizer = SVCFaceRecognizerForCamera(model_path, cam_index)
green_led = None
red_led = None

while True:
    (found, name, percentage) = recognizer.camera_match()
    if found == False:
        print('No face found or Error occured')
        time.sleep(1)
        continue

    if percentage >= args.match_percentage:
        print('Welcome ' + name)
        if green_led is None:
            green_led = gpiozero.LED(green_pin)
        green_led.on()
        time.sleep(1)
        green_led.off()
    else:
        if red_led is None:
            red_led = gpiozero.LED(red_pin)
        red_led.on()
        time.sleep(1)
        red_led.off()
