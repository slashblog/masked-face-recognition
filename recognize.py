from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Normalizer
from sklearn.svm import SVC
from embedding import Embedding, MaskedEmbedding, MyriadEmbedding
from face import FaceNotFoundException
from PIL import Image, ImageOps

import argparse
import cv2
import joblib
import numpy
import os
import time

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

class SVCFaceRecognizer:
    def __init__(self, model_path, use_myriad = False):
        self.embedding = MyriadEmbedding() if use_myriad else Embedding()
        self.model = joblib.load(model_path)
        self.out_encoder = LabelEncoder()
        self.out_encoder.classes_ = numpy.load(model_path + '-classes.npy')
        print('Possible outcomes: ')
        print(self.out_encoder.classes_)

    def match(self, image):
        image_description = image if type(image) == str else 'from camera'
        try:
            match_start = start = time.time()
            face_embedding = self.embedding.get(image)
            end = time.time()
            print(f'Extract embedding took: {end - start}')
        except FaceNotFoundException:
            print('No face found in: ' + image_description)
            return False, None, None, 0, time.time() - match_start

        in_encoder = Normalizer(norm='l2')
        face_embedding = in_encoder.transform(face_embedding)

        #yhat_class = self.model.predict(face_embedding)
        start = time.time()
        yhat_prob = self.model.predict_proba(face_embedding)
        end = time.time()
        print(f'predict_proba took: {end - start}')
        # get name

        yhat_class = [numpy.argmax(numpy.array(yhat_prob))]

        #print(yhat_class)
        #print(yhat_prob)
        
        class_index = yhat_class[0]
        class_probability = yhat_prob[0,class_index] * 100

        probabilities = numpy.array(yhat_prob).flatten()
        probabilities.sort()
        probabilities = probabilities[::-1]
        print(probabilities)
        if probabilities[0] < probabilities[1] + probabilities[2]:
            print('For image: %s, No known user match found' % image_description)
            return True, ['unknown'], None, 0, time.time() - match_start

        predict_names = self.out_encoder.inverse_transform(yhat_class)
        #print('Predicted: %s (%.3f)' % (predict_names[0], class_probability))
        #print('For image: %s, Predicted user is: %s' % (image_description, predict_names[0]))

        return True, predict_names, class_index, class_probability, time.time() - match_start

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Face recognizer')
    parser.add_argument('embed', type=str, help='Model and Embedding identifier')
    parser.add_argument('--io', required=False, default=False, action='store_true', help='Use GPIO')
    parser.add_argument('--myriad', required=False, default=False, action='store_true', help='Use Intel Movidius Neural Compute Stick')
    parser.add_argument('--gui', required=False, default=False, action='store_true', help='Show images with labels.')

    subparsers = parser.add_subparsers(help='sub command help')

    camera_parser = subparsers.add_parser('camera', help='camera help')
    camera_parser.add_argument('--min-match', type=int, default=50, choices=range(0,100), help='Minumum percentage required for successful match')
    camera_parser.add_argument('camera', type=int, nargs='?', default=0, help='Camera number to be used for input')

    images_parser = subparsers.add_parser('images', help='images help')
    images_parser.add_argument('--no-randomize', required=False, action='store_true', help='Do not randomize images')
    images_parser.add_argument('images', type=str, nargs='+', help='Image paths')

    args = parser.parse_args()

    embed = args.embed
    model_path = embed + '-model'

    if args.io:
        from gpiozero.pins.native import NativeFactory
        from gpiozero import Device, LED
        Device.pin_factory = NativeFactory()

        red_led = LED(22)
        green_led = LED(23)
        white_led = LED(17)
        yellow_led = LED(27)

        red_led.off()
        green_led.off()
        yellow_led.off()
        white_led.off()

    if args.gui:
        import matplotlib
        from matplotlib import pyplot
        matplotlib.use('TkAgg')

    recognizer = SVCFaceRecognizer(model_path, args.myriad)

    if hasattr(args, 'images'):
        from prettytable import PrettyTable
        table = PrettyTable(['Image', 'Name', 'Probability', 'Accuracy', 'Duration'])
        images_list = args.images

        if not args.no_randomize:
            import random
            random.shuffle(images_list)

        distribution_count = {
            '00 - 40': 0,
            '40 - 60': 0,
            '60 - 80': 0,
            '80 - 90': 0,
            '90+': 0
        }
        start_time = time.time()
    
        for image in images_list:
            if not os.path.exists(image):
                print('Skipping file ' + image + ' as it does not exist')
                continue

            if args.io:
                white_led.on()
                #time.sleep(1)

                white_led.off()
                yellow_led.on()

            start = time.time()
            exist, name, index, class_probability, duration = recognizer.match(image)
            end = time.time()

            if exist:
                accuracy = '90+' if class_probability > 90 else '80 - 90' if class_probability > 80 else '60 - 80' if class_probability > 60 else '40 - 60' if class_probability > 40 else '00 - 40'
                distribution_count[accuracy] = distribution_count[accuracy] + 1
                table.add_row([ image, name[0], '%.2f' % class_probability, accuracy, duration ])
            else:
                table.add_row([ image, 'No face found', '--.--', '--', duration ])

            print(f"Image recognition took {end - start}")

            if args.io:
                yellow_led.off()
                if not exist or name == 'unknown':
                    red_led.on()
                else:
                    green_led.on()

            if args.gui:
                # load image from file
                pixels = Image.open(image)
                pixels = ImageOps.exif_transpose(pixels)
                # convert to RGB, if needed
                pixels = pixels.convert('RGB')
                # convert to array
                pixels = numpy.asarray(pixels)

                pyplot.imshow(pixels)
                if exist:
                    pyplot.title('%s [%f seconds]' % (name, end - start))
                else:
                    pyplot.title('No face found [%f seconds]' % (end - start))
                pyplot.show()
            
            if args.io:
                time.sleep(10)
                green_led.off()
                red_led.off()
                white_led.on()
        
        print(table)
        
        table = PrettyTable(['Range', 'Count'])
        for key in distribution_count:
            table.add_row([ key, distribution_count[key] ])
        print(table)

        print('Total %d images processed in %0.2f seconds' % (len(images_list), time.time() - start_time))
    else:
        if args.io:
            #white_led.on()
            red_led.on()

        print('Initializing camera')
        try:
            cam = cv2.VideoCapture(args.camera)
            cam.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
            cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        except:
            print('Failed to open camera with index: %d' % args.camera)
            exit(1)

        count = 0
        while count < 3:
            s, img = cam.read()
            if s:
                #if args.io:
                    #time.sleep(1)
                    #white_led.off()
                    #yellow_led.on()

                is_match = False
                count = 0
                start = time.time()
                exist, name, index, class_probability, duration = recognizer.match(img)
                end = time.time()

                if not exist or class_probability < args.min_match:
                    print('Low probability match ignoring')
                    name = [ 'unknown' ]
                    is_match = False
                else:
                    is_match = True

                if args.io:
                    #yellow_led.off()
                    if is_match:
                        #print('Turn off red and turn on green')
                        red_led.off()
                        green_led.on()

                if exist:
                    title = '%s [%f seconds, %.2f]' % (name, end - start, class_probability)
                else:
                    title = 'No face found [%f seconds]' % (end - start)

                print(title)

                if args.gui:
                    cv2.namedWindow('frame')
                    cv2.setWindowTitle('frame', title)
                    cv2.imshow('frame', img)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                
                if args.io:
                    if is_match:
                        #print('Sleeping on match')
                        time.sleep(5)
                        #print('Turn off green and turn on red')
                        green_led.off()
                        red_led.on()
                    #white_led.on()
            else:
                count = count + 1

        cam.release()

        if count >= 3:
            print('Unable to read image from camera with index: %d' % args.camera)
            exit (1)

    #print('Performing camera match')
    #recognizer = SVCFaceRecognizerForCamera(model_path)
    #recognizer.camera_match()
